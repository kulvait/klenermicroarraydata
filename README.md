# klenerMicroarrayData #

### Package content ###

* R package with Illumina microarrays data from Pavel Klener experiments. 
* Data objects are WEHA UPF1 RUL and annotationHumanHT12V4.
*  This package is intended to be processed by beadarray and blima Bioconductor packages.

### Setup ###

To install it in R just type


```
#!R

library(devtools)
install_bitbucket("kulvait/klenerMicroarrayData")
```

### Contribution guidelines ###

* You are welcome to review repository and code.

### Maintainer ###

* Vojtěch Kulvait
