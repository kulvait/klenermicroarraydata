\name{klenerMicroarrayData-package}
\alias{klenerMicroarrayData-package}
\alias{klenerMicroarrayData}
\docType{package}
\title{Data from microarrays from Pavel Klener experiments.}

\description{Experiment data package. Data from microarrays from Pavel Klener experiments. Data objects are WEHA UPF1 RUL and annotationHumanHT12V4.}

\details{

The DESCRIPTION file:
\packageDESCRIPTION{klenerMicroarrayData}
\packageIndices{klenerMicroarrayData}
}
\author{
\packageAuthor{klenerMicroarrayData}

Maintainer: \packageMaintainer{klenerMicroarrayData}
}

\keyword{ package }
